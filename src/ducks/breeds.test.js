import reducer from '.'
import { RECEIVE_BREEDS } from './breeds'

describe('breeds reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      'breedImage': '',
      'breeds': [],
      'selectedBreed': ''
    })
  })

  it('should handle RECEIVE_BREEDS', () => {
    expect(
      reducer(undefined, {
        type: RECEIVE_BREEDS,
        payload: {
          message: {
            'briard': [],
            'bulldog': ['french']
          }
        }
      })
    ).toEqual({
      'breedImage': '',
      'breeds': [
        {'key': 'briard', 'text': 'Briard', 'value': 'briard'},
        {'key': 'bulldog/french', 'text': 'French Bulldog', 'value': 'bulldog/french'}
      ],
      'selectedBreed': ''
    })
  })

  // TODO: write tests for the rest of reducers
})
