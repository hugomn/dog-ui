import React from 'react'
import { Image } from 'semantic-ui-react'

const BreedImage = ({src}) => {
  if (process.env.NODE_ENV !== 'production') {
    console.log('BreedImage render')
  }
  return <Image src={src} centered />
}

export default BreedImage
