import React from 'react'
import renderer from 'react-test-renderer'
import BreedImage from './BreedImage'

it('BreedImage renders correctly', () => {
  const tree = renderer
    .create(<BreedImage src='' />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
