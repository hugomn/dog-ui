import React from 'react'
import { Dropdown } from 'semantic-ui-react'

const BreedSelect = ({ breeds, selectBreed, selectedBreed }) => {
  if (process.env.NODE_ENV !== 'production') {
    console.log('BreedSelect render')
  }
  return (
    <Dropdown
      defaultValue={selectedBreed}
      loading={!breeds.length}
      onChange={selectBreed}
      options={breeds}
      placeholder='Select breed...'
      search selection
    />
  )
}

export default BreedSelect
