# Dog UI

![](http://dog.ceo/img/dog-api-logo.svg)

[![pipeline status](https://gitlab.com/eloypnd/dog-ui/badges/master/pipeline.svg)](https://gitlab.com/eloypnd/dog-ui/commits/master)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

This project is a playground based on a Case Study for a recruitment process. Goal is to showcase different architecture alternatives for a UI based on [ReactJS](https://reactjs.org).

This project was bootstrapped with [Create React App](./docs/create-react-app.md).

See it running here: [eloypnd.gitlab.io/dog-ui](https://eloypnd.gitlab.io/dog-ui).

## Usage

In the project directory, you can run:

- `npm install`<br>
    Run this first to get all dependencies.

- `npm start`<br>
    Runs the app in the development mode.<br>
    > Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br>
    The page will reload if you make edits.<br>
    You will also see any lint errors in the console.

- `npm test`<br>
    Launches the test runner in the interactive watch mode.

- `npm run build`<br>
    Builds the app for production to the `build` folder.<br>

## Architecture

I decided to make different versions of the Dog UI to compare different architecture decisions and how they may affect performance, testability, developer experience, ...

Initially there are 2 basic versions:

### 1. Minimal with stateful components

> To run this version: `git checkout v1.0`

- We use the minium possible libraries.
- Each component keep its own state.<br>
    - Only the compoent affected by a state change re-renders.
    - If performance is key, this is important to consider.

### 2. Minimal, with stateless components

> To run this version: `git checkout v2.0`

- We use the minium possible libraries.
- We have one single component with state and all other components below are simple presentational components.
    - This make all the other components much more simple, easy to reason and test.
    - It adds some overhead to pass down through props the state and actions.

### 3. Let's image this is a more complex app

> To run this version: `git checkout v3.0`

- We implement an architecture for a more complex app using [redux](https://redux.js.org/) to manage the state.
- We use [ducks](https://github.com/erikras/ducks-modular-redux/) to organise actions, reducers, side-effects, etc.
- We'll keep it simple for this exercise and use [redux-thunk](https://github.com/gaearon/redux-thunk).<br>
    There are more sophisticated options like [redux-observable](https://redux-observable.js.org) or [redux-saga](https://redux-saga.js.org).
